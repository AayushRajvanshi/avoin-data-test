import React from 'react';
import { connect } from 'react-redux';
import { setBusinessId, setCompanyName, fetchCompany } from './../actions/index';

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            businessId: '',
            companyName: ''
        }
    }

    handleBusinessIdChange = (e) => {
        e.preventDefault();
        const businessId = e.target.value;
        this.props.setBusinessId(businessId);
    }

    handleCompanyNameChange = (e) => {
        e.preventDefault();
        const companyName = e.target.value;
        this.props.setCompanyName(companyName);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.fetchCompany();
    }

    render() {
        const { businessId, companyName } = this.props;
        return (
            <form className="uk-form-stacked uk-margin" onSubmit={this.handleSubmit}>
                <div className="uk-margin">
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-horizontal-text" type="text" placeholder="Enter Business Id" value={businessId} onChange={this.handleBusinessIdChange} />
                    </div>
                </div>
                <div className="uk-margin">
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-horizontal-text" type="text" placeholder="Enter Company Name" value={companyName} onChange={this.handleCompanyNameChange} />
                    </div>
                </div>
                <button className="uk-button uk-button-secondary uk-width-1-1" type="submit">Submit</button>
            </form>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const { businessId, companyName } = state.companies;
    return {
        businessId,
        companyName
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setBusinessId: (businessId) => {
            dispatch(setBusinessId(businessId))
        },
        setCompanyName: (companyName) => {
            dispatch(setCompanyName(companyName))
        },
        fetchCompany: () => {
            dispatch(fetchCompany())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Input);