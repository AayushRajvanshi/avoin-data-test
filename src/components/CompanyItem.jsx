import React from 'react';

const CompanyItem = ({ name, businessId, registrationDate, companyForm }) => {
    return (
        <div>
            <div className="uk-card uk-card-default uk-card-body uk-text-center">
                <h3 className="uk-card-title">{name}</h3>
                <p>Business Id - {businessId}</p>
                <p>Registration Date - {registrationDate}</p>
                <p>Company Form - {companyForm}</p>
            </div>
        </div>
    );
}

export default CompanyItem;