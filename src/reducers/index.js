import { combineReducers } from 'redux';

export var companyReducer = (state = {
    'businessId': '',
    'companyName': '',
    'maxResults': 12,
    'resultsFrom': 0,
    'totalPages': 0,
    'currentPage': 1,
    'isFetching': false,
    'companies': undefined
}, action) => {
    switch (action.type) {
        case 'SET_BUSINESS_ID':
            return {
                ...state,
                businessId: action.businessId
            };
        case 'SET_COMPANY_NAME':
            return {
                ...state,
                companyName: action.companyName
            };
        case 'START_COMPANY_FETCH':
            return {
                ...state,
                isFetching: true,
                companies: undefined,
                currentPage: 1,
                resultsFrom: 0,
                totalPages: 0
            };
        case 'COMPLETE_COMPANY_FETCH_SUCCESS':
            return {
                ...state,
                isFetching: false,
                companies: action.data,
                totalPages: Math.ceil(action.data.totalResults / state.maxResults)
            };
        case 'COMPLETE_COMPANY_FETCH_FAILURE':
            return {
                ...state,
                isFetching: false,
                companies: action.data
            };
        case 'START_CHANGE_PAGE':
            return {
                ...state,
                isFetching: true,
                currentPage: state.currentPage + action.data
            };
        case 'SET_RESULTS_FROM':
            return {
                ...state,
                resultsFrom: state.maxResults * (state.currentPage - 1)
            };
        default:
            return state;
    }
};


const reducer = combineReducers({
    companies: companyReducer
});


export default reducer;