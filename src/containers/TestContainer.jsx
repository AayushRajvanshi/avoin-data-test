import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from './../components/Input.jsx';
import CompanyItem from './../components/CompanyItem.jsx';
import CompanyList from './../components/CompanyList.jsx';
import { setResultsFrom, fetchCompany, changePage } from './../actions/index';

class TestContainer extends Component {
    handlePrevious = () => {
        this.props.changePage(-1);
    }
    handleNext = () => {
        this.props.changePage(1);
    }
    render() {
        const { isFetching, companies, currentPage, totalPages } = this.props;
        const companiesList = companies ? companies.results.length > 0 ? companies.results.map((company, index) => {
            return <CompanyItem key={index} {...company} />
        }) : <div className="uk-text-center">No result Found</div> : null;
        const node = isFetching ? <div className="uk-section uk-section-default uk-height-large uk-text-center">Loading...</div> :
            <CompanyList >
                {companiesList}
            </CompanyList >
        return (
            <div>
                <div className="uk-section uk-section-default uk-preserve-color">
                    <div className="uk-container uk-container-small">
                        <div className="uk-h3 uk-text-uppercase uk-text-center">avoin data</div>
                        <Input />
                        {companies && companies.results.length > 0 ? <hr /> : null}
                        {companies && companies.results.length > 0 ? <div className="uk-margin uk-flex uk-flex-between">
                            <button className="uk-button uk-button-secondary" disabled={currentPage === 1} onClick={this.handlePrevious}>Previous</button>
                            <button className="uk-button uk-button-secondary" disabled={currentPage === totalPages} onClick={this.handleNext}>Next</button>
                        </div> : null}
                        {node}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { isFetching, maxResults, companies, currentPage, totalPages } = state.companies;
    return {
        isFetching,
        companies,
        maxResults,
        currentPage,
        totalPages
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changePage: (by) => {
            dispatch(changePage(by))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestContainer);