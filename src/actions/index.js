import axios from 'axios';

export const setBusinessId = (businessId) => {
    return {
        type: 'SET_BUSINESS_ID',
        businessId
    };
};


export const setCompanyName = (companyName) => {
    return {
        type: 'SET_COMPANY_NAME',
        companyName
    };
};

const startCompanyFetch = () => {
    return {
        type: 'START_COMPANY_FETCH'
    };
};

const completeCompanyFetchSuccess = (data) => {
    return {
        type: 'COMPLETE_COMPANY_FETCH_SUCCESS',
        data
    };
};
const completeCompanyFetchFailure = (data) => {
    return {
        type: 'COMPLETE_COMPANY_FETCH_FAILURE',
        data
    };
};

export const fetchCompany = () => {
    return (dispatch, getState) => {
        dispatch(startCompanyFetch());
        const businessId = getState().companies.businessId;
        const companyName = getState().companies.companyName;
        const resultsFrom = getState().companies.resultsFrom;
        const maxResults = getState().companies.maxResults;
        const url = `/api?businessId=${businessId}&name=${companyName}&resultsFrom=${resultsFrom}&maxResults=${maxResults}`;
        axios.get(url).then((res) => {
            dispatch(completeCompanyFetchSuccess(res.data));
        }).catch((err) => {
            dispatch(completeCompanyFetchFailure());
            console.log(err);
        });
    };
};

const setResultsFrom = (resultsFrom) => {
    return {
        type: 'SET_RESULTS_FROM',
        resultsFrom
    };
};



const startChangePage = (data) => {
    return {
        type: 'START_CHANGE_PAGE',
        data
    };
};

export const changePage = (data) => {
    return (dispatch, getState) => {
        dispatch(startChangePage(data));
        dispatch(setResultsFrom());
        const businessId = getState().companies.businessId;
        const companyName = getState().companies.companyName;
        const resultsFrom = getState().companies.resultsFrom;
        const maxResults = getState().companies.maxResults;
        const url = `/api?businessId=${businessId}&name=${companyName}&resultsFrom=${resultsFrom}&maxResults=${maxResults}`;
        axios.get(url).then((res) => {
            dispatch(completeCompanyFetchSuccess(res.data));
        }).catch((err) => {
            dispatch(completeCompanyFetchFailure());
            console.log(err);
        });
    };
};